import { Role } from "../enums/roles.enum";

export interface INavItem {
  icon: string;
  title: string;
  link?: string;
  roles?: Role[];
}
