import React from 'react';
import MainHeader from './layouts/main-header/main-header';
import './styles/main.scss';
import { IUser } from "./models/interfaces/user.interface";
import { user as cUser } from './mock-data/mock-data'
import { Role } from "./models/enums/roles.enum";
import SideNav from "./layouts/side-nav/side-nav";
import { Route, BrowserRouter, Routes } from "react-router-dom";
import NotFoundPage from "./pages/not-found-page";
import Posts from "./pages/posts";
import UserData from "./pages/user-data";

function App() {
  let currentUser: IUser = cUser;
  let currentRole: Role = Role.user;

  return (
    <BrowserRouter>
      <div className="App">
        <MainHeader />

        <div className="content">
          <SideNav currentRole={currentRole} currentUser={currentUser} />

          <Routes>
            <Route path='/user-data' element={<UserData/>} />
            <Route path='/posts' element={<Posts/>} />
            <Route path='/**' element={<NotFoundPage/>} />
          </Routes>

        </div>

      </div>
    </BrowserRouter>
  );
}

export default App;
