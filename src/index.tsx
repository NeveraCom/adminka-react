import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import i18next from 'i18next';
import { I18nextProvider } from "react-i18next";
import en from "./assets/i18n/en.json";
import ua from "./assets/i18n/ua.json";

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

i18next.init({
  interpolation: { escapeValue: false },  // React already does escaping
  lng: 'ua',                              // language to use
  resources: {
    en: {
      en: en
    },
    ua: {
      ua: ua
    },
  },
});

root.render(
  <I18nextProvider i18n={i18next}>
    <App />
  </I18nextProvider>
);
