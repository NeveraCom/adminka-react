import React, {FC} from 'react';
import CustomIcon from "../../components/custom-icon";
import LightModeSwitcher from "../../components/light-mode-switcher";
import { IconButton } from "@mui/material";
import ChangeLang from "../../components/change-lang";

const MainHeader: FC = function() {
  return (
    <header className="header">
      <div className="header__content">
        <a className="header__logo" href="/">
          <div className="header__logo-img">
            <CustomIcon svgIcon={'logo'}/>
          </div>
          <span className="header__logo-text">Devias Kit</span>
        </a>

        <div className="header__button-group">
          <LightModeSwitcher></LightModeSwitcher>

          <ChangeLang />

          {/*<button className="custom-button custom-button_icon header__button ">*/}
          {/*  <CustomIcon svgIcon={'logout'}></CustomIcon>*/}
          {/*</button>*/}

          <IconButton color="primary" aria-label="upload picture" component="label">
            {/* TODO: clear then done unlogin */}
            {/*<input hidden accept="image/*" type="file" />*/}
            {/*<PhotoCamera />*/}
            <CustomIcon svgIcon={'logout'}></CustomIcon>
          </IconButton>
        </div>
      </div>
    </header>
  )
}

export default MainHeader;
