import React from 'react';
import CustomIcon from "../../components/custom-icon";
import {IUser} from "../../models/interfaces/user.interface";

interface ISideNavUsersProps {
  currentUser: IUser;
}

const SideNavUser = ({currentUser}: ISideNavUsersProps) => {
  const name: string = currentUser.name;
  const company: string = currentUser.company.name;

  return (
    <div className="user">
      <div className="user__img-wrapper">
        <CustomIcon svgIcon={'user-outline'} />
      </div>

      <div className="user__name">{name}</div>
      <div className="user__company">{company}</div>
    </div>
  );
};

export default SideNavUser;
