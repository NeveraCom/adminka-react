import React from 'react';
import SideNavUser from "./side-nav-user";
import SideNavList from "./side-nav-list";
import { IUser } from "../../models/interfaces/user.interface";
import { Role } from "../../models/enums/roles.enum";

interface ISideNavProps {
  currentUser: IUser;
  currentRole: Role;
}

const SideNav = ({currentRole, currentUser}: ISideNavProps) => {
  return (
    <div className='sidebar'>
      <SideNavUser currentUser={currentUser} />
      <SideNavList currentRole={currentRole} />
    </div>
  );
};

export default SideNav;
