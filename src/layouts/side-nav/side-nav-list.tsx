import React from 'react';
import { Role } from "../../models/enums/roles.enum";
import { INavItem } from "../../models/interfaces/nav-item.interface";
import CustomIcon from "../../components/custom-icon";
import { useTranslation } from "react-i18next";

interface ISideNavListProps {
  currentRole: Role;
}

const SideNavList = ({currentRole}: ISideNavListProps) => {
  let navItemsShow: INavItem[];
  const navItems: INavItem[] = [
    {
      icon: 'users',
      title: 'USERS',
      link: '/users',
      roles: [Role.user, Role.admin]
    },
    {
      icon: 'users',
      title: 'MY_DATA',
      link: '/user-data',
      roles: [Role.user]
    },
    {
      icon: 'posts',
      title: 'MY_POSTS',
      link: '/posts',
      roles: [Role.user]
    },
    {
      icon: 'photo',
      title: 'MY_ALBUMS',
      link: '/albums',
      roles: [Role.user]
    },
    {
      icon: 'text',
      title: 'MY_TODOS',
      link: '/todos',
      roles: [Role.user]
    },
    {
      icon: 'chart',
      title: 'MY_CHART',
      link: '/statistics',
      roles: [Role.user]
    }
  ];

  navItemsShow = navItems.filter(el => el.roles?.includes(currentRole));

  const { t } = useTranslation('ua');

  return (
    <nav className={"nav"}>
      <ul className={"nav__list"}>
        {navItemsShow.map(item =>
          <li className={"nav__list-item"} key={item.title}>
            <a className={"nav__link"}>
              <span className={'nav__icon'}>
                <CustomIcon svgIcon={item.icon} />
              </span>
              <span>{ t('NAV.' + item.title) }</span>
            </a>
          </li>
        )}
      </ul>
    </nav>
  );
};

export default SideNavList;
