import React from 'react';

const SlideToggle = () => {
  return (
    <div className={'slide-toggle'}>
      <input type="checkbox" id="switch"/>
      <label htmlFor="switch"></label>
    </div>
  );
};

export default SlideToggle;
