import React from 'react';
import { ReactComponent as Arrow } from './../assets/images/svg/arrow.svg';
import { ReactComponent as ArrowRight } from './../assets/images/svg/arrow-right.svg';
import { ReactComponent as Chart } from './../assets/images/svg/chart.svg';
import { ReactComponent as Dots } from './../assets/images/svg/dots.svg';
import { ReactComponent as Edit } from './../assets/images/svg/edit.svg';
import { ReactComponent as Logo } from './../assets/images/svg/logo.svg';
import { ReactComponent as LogoDark } from './../assets/images/svg/logo-dark.svg';
import { ReactComponent as Logout } from './../assets/images/svg/logout.svg';
import { ReactComponent as Moon } from './../assets/images/svg/moon.svg';
import { ReactComponent as Photo } from './../assets/images/svg/photo.svg';
import { ReactComponent as Posts } from './../assets/images/svg/posts.svg';
import { ReactComponent as Sun } from './../assets/images/svg/sun.svg';
import { ReactComponent as Text } from './../assets/images/svg/text.svg';
import { ReactComponent as Users } from './../assets/images/svg/users.svg';
import { ReactComponent as UserOutline } from './../assets/images/svg/user-outline.svg';

interface ICustomIconProps {
  svgIcon: string;
  className?: string;
}

const CustomIcon = ({svgIcon}: ICustomIconProps) => {
  switch (svgIcon) {
    case 'arrow': return <Arrow />;
    case 'arrow-right': return <ArrowRight />;
    case 'charts': return <Chart />;
    case 'dots': return <Dots />;
    case 'edit': return <Edit />;
    case 'logo': return <Logo />;
    case 'logo-dark': return <LogoDark />;
    case 'logout': return <Logout />;
    case 'moon': return <Moon />;
    case 'photo': return <Photo />;
    case 'posts': return <Posts />;
    case 'sun': return <Sun />;
    case 'text': return <Text />;
    case 'users': return <Users />;
    case 'user-outline': return <UserOutline />;
    default: return <Logo />;
  }
}

export default CustomIcon;
