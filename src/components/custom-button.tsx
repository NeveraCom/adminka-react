import React from 'react';

interface IButton {
  type?: 'icon' | 'text';
  color?: 'primary' | 'transparent';
  size?: 'medium';
  children?: any
}

const CustomButton = ({children, type = 'text', color, size = 'medium'}: IButton) => {
  const classes: string = `custom-button custom-button_${type} custom-button_${color} custom-button_${size}`;

  return (
    <button className={classes}>
      {children}
    </button>
  );
};

export default CustomButton;


