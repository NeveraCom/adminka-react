import React from 'react';
import CustomIcon from "./custom-icon";
import SlideToggle from "./slide-toggle";

const LightModeSwitcher = () => {
  return (
    <div className="mode-switcher">
      <div className="mode-switcher__icon-wrapper">
        <CustomIcon svgIcon={'sun'} />
      </div>
      <SlideToggle></SlideToggle>
      <div className="mode-switcher__icon-wrapper">
        <CustomIcon svgIcon={'moon'} />
      </div>
    </div>
  );
};

export default LightModeSwitcher;